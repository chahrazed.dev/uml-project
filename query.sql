-- Active: 1709647914689@@127.0.0.1@3306@uml
SHOW DATABASES;

SHOW TABLES;


-- USE CASE 1

-- LISTER LES SALLES D'UN CENTRE
SELECT *
FROM room
JOIN center ON room.center_id = center.id
WHERE center.name = "Bordeaux";

-- ASSIGNER UNE CAPACITE A UNE SALLE
UPDATE room
SET capacity = 18
WHERE name = "CSS" AND center_id = 1;

-- LISTER LES CENTRES D'UNE REGION
SELECT center.name 
FROM center 
INNER JOIN region ON center.region_id = region.id
WHERE region.name = 'ARA';



-- USE CASE 2

-- LISTER LES PROMOS D'UN CENTRE
SELECT *
FROM promo
JOIN center ON promo.center_id = center.id
WHERE center.name = "Lyon";


-- MODIFIER LA LISTE DES APPRENANTS
UPDATE student
SET name = "Bloup"
WHERE id = 1;

SELECT * from student;


-- AFFICHER LE PLANNING COMPLET PAR SALLE
SELECT room.name AS 'Salle', booking.start_date AS 'Début', booking.end_date AS 'Fin', promo.name AS 'Promotion'
FROM booking
JOIN room ON booking.room_id = room.id
JOIN promo ON booking.promo_id = promo.id;

-- AFFICHER LE PLANNING COMPLET PAR PROMO


-- MODIFIER UNE RESERVATION
UPDATE booking SET start_date = '2005-04-21', end_date = '2006-01-15' 
WHERE promo_id = 1;

-- SUPPRIMER UNE RESERVATION

DELETE FROM booking
WHERE id = 2;