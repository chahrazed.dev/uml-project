-- Active: 1709647914689@@127.0.0.1@3306@uml
DROP TABLE IF EXISTS promo_student;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS region ;


CREATE TABLE region (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(266)
);

CREATE TABLE center (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(266),
    region_id INT,
    Foreign Key (region_id) REFERENCES region(id) ON DELETE SET NULL
);

CREATE TABLE room (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(266),
    capacity INT,
    color VARCHAR(64),
    center_id INT,
    Foreign Key (center_id) REFERENCES center(id)
);

CREATE TABLE promo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (266),
    start_date DATE,
    end_date  DATE,
    student_number INT,
    center_id INT,
    Foreign Key (center_id) REFERENCES center(id)
);

CREATE TABLE booking (
    id INT PRIMARY KEY AUTO_INCREMENT,
    start_date DATETIME,
    end_date DATETIME,
    room_id INT,
    promo_id INT,
    Foreign Key (room_id) REFERENCES room(id),
    Foreign Key (promo_id) REFERENCES promo(id)
);

CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(266)
);

CREATE TABLE promo_student (
    promo_id INT,
    student_id INT,
    PRIMARY KEY (promo_id, student_id),
    Foreign Key (promo_id) REFERENCES promo(id) ON DELETE CASCADE,
    Foreign Key (student_id) REFERENCES student(id) ON DELETE CASCADE
);


INSERT INTO region (name) VALUES 
("ARA"),
("ILE DE FRANCE"), 
("AQUITAINE");

INSERT INTO center (name, region_id) VALUES 
("Lyon", 1),
("Paris", 2),
("Bordeaux", 3);

INSERT INTO room (name, capacity, color, center_id) VALUES
("CSS", 25, "red", 1),
("Symfony", 18, "blue", 1),
("Java", 20, "white", 1),
("CSS", 25, "red", 2),
("Symfony", 18, "blue", 2),
("Java", 20, "white", 2),
("CSS", 25, "red", 3),
("Symfony", 18, "blue", 3),
("Java", 20, "white", 3);

INSERT INTO promo (name, start_date, end_date, student_number, center_id) VALUES
("DWWB", "2024-01-08", "2024-12-06", 15, 1),
("DWWB", "2024-02-20", "2025-01-10", 14, 2),
("DWWB", "2023-11-20", "2024-10-21", 15, 3),
("AFP", "2024-03-07", "2024-04-06", 13, 1),
("AFP", "2024-04-12", "2024-05-10", 12, 2),
("AFP", "2024-09-23", "2024-11-20", 12, 3);

INSERT INTO booking (start_date, end_date, room_id, promo_id) VALUES
("2024-01-01", "2024-12-31", 1, 2),
("2024-03-01", "2024-10-31", 2, 3),
("2024-05-01", "2024-09-30", 3, 2);

INSERT INTO student (name) VALUES
("Souad"),
("Aymeric"),
("Chieko"),
("Felix"),
("Jules"),
("Océane");

INSERT INTO promo_student (promo_id, student_id) VALUES
(1,1),
(1,2),
(2,3),
(2,4),
(3,5),
(3,6);